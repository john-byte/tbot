const TelegramBotAPI = require('node-telegram-bot-api');
const background = require("./background");
const atof = require("./utils").atof;

/**
 * 
 * @param {(msg: TelegramBotAPI.Message) => boolean} predicate 
 * @param {number?} initCount
 */
const makeConditionalCounter = (predicate, initCount) => {
    let count = initCount || 0;
    const getCount = () => count;
    const mayIncrement = (msg) => {
        if (predicate(msg)) {
            return ++count;
        }
    };
    const mayDecrement = (msg) => {
        if (predicate(msg)) {
            return --count;
        }
    }

    return {
        getCount,
        mayIncrement,
        mayDecrement,
    }
};

const initBot = async () => {
    // const token = "428976315:AAGl3N-jd1yGjyGbY4o45RNaVU6ISVUeDGc";
    const token = "1847679573:AAFa1XNh84sK4toJbvs4V1fjZ6NNp4Bf82c";
    const bot = new TelegramBotAPI(token, { polling: true });

    let counters = undefined;
    try {
        counters = await background.loadCounters();
    } catch (_) { }

    const emailRegex = /[a-z0-9_\-\.]+@[a-z0-9_\-\.]+/ig;
    const msgWEmailsCounter = makeConditionalCounter(
        (msg) => !!msg.text.match(emailRegex),
        (counters ? counters.withEmails : 0)
    );

    const linkRegex = /https?:\/\/[\w\.]+\.[a-z]{2,6}\.?(?:\/[\w\.]*)*\/?/ig;
    const msgWLinksCounter = makeConditionalCounter(
        (msg) => !!msg.text.match(linkRegex),
        (counters ? counters.withLinks : 0)
    );

    const pingRegex = /@[a-z0-9_\-]+/ig;
    const msgWPings = makeConditionalCounter(
        (msg) => !!msg.text.match(pingRegex),
        (counters ? counters.withPings : 0)
    );

    const msgWMentionsCounter = makeConditionalCounter(
        (msg) => !!msg.reply_to_message,
        (counters ? counters.withMentions : 0)
    );

    const msgWSumOver10KCounter = makeConditionalCounter(
        (msg) => {
            let sum = 0;
            let i = 0;
            while (i < msg.text.length) {
                const [num, nextId] = atof(msg.text, i);
                sum += num;
                i = nextId;
            }
            console.log("Sum => ", sum);
            return sum > 10_000;
        },
        (counters ? counters.withSumOver10K : 0)
    );

    const msgTotalCounter = makeConditionalCounter(
        _ => true,
        (counters ? counters.total : 0)
    );

    //commands
    const HI_COMMAND = /^hi$/ig;
    const ABOUT_COMMAND = /^about$/ig;
    const DATA_COMMAND = /^data$/ig;
    const GOODBYE_COMMAND = /^goodbye$/ig;
    const STATS_COMMAND = "!stats";

    const censorshipRegex = /shit|fuck|motherfucker|bitch|whore/gi;

    console.log("Bot successfully started!");
    bot.on('message', async (msg) => {
        try {
            // Hooks for counting messages
            msgTotalCounter.mayIncrement(msg);
            msgWEmailsCounter.mayIncrement(msg);
            msgWLinksCounter.mayIncrement(msg);
            msgWMentionsCounter.mayIncrement(msg)
            msgWSumOver10KCounter.mayIncrement(msg);
            msgWPings.mayIncrement(msg);

            //chat_ID
            const chatId = msg.chat.id;

            if (msg.text.match(HI_COMMAND)) {
                bot.sendMessage(chatId, `Hello, ${msg.from.first_name}! 💬 📧`);
            }

            if (msg.text.match(ABOUT_COMMAND)) {
                bot.sendMessage(
                    msg.chat.id,
                    (
                        `
                        <b>Chat ID</b>: ${msg.chat.id} 
                        \n<b>Your PERSONAL ID</b>: ${msg.from.id}
                        \n<b>Name</b>: ${msg.from.first_name || ""} ${msg.from.last_name || ""}
                    `
                    ),
                    { parse_mode: "HTML" }
                );
            }

            if (msg.text.match(DATA_COMMAND)) {
                bot.sendMessage(chatId, `JSON.stringify(msg) = ${JSON.stringify(msg, null, 2)}`);
            }

            if (msg.text.match(GOODBYE_COMMAND)) {
                bot.sendMessage(chatId, `goodbye, ${msg.from.first_name}!`);
            }

            if (msg.text === STATS_COMMAND) {
                bot.sendMessage(
                    chatId,
                    `
                    <b>Total messages</b>: ${msgTotalCounter.getCount()}
                    <b>With emails</b>: ${msgWEmailsCounter.getCount()}
                    <b>With links</>: ${msgWLinksCounter.getCount()}
                    <b>With mentions</b>: ${msgWMentionsCounter.getCount()}
                    <b>With numbers more than 10K</b>: ${msgWSumOver10KCounter.getCount()}
                    <b>With pings</b>: ${msgWPings.getCount()}
                `,
                    { parse_mode: "HTML" }
                )
            }

            if (msg.text.match(censorshipRegex)) {
                bot.sendMessage(
                    chatId,
                    "Censorship! And your message will be removed after 2 seconds."
                ).then((result) => {
                    setTimeout(() => {
                        bot.deleteMessage(chatId, result.message_id);
                        bot.deleteMessage(chatId, msg.message_id);

                        // Hooks for counting messages
                        msgTotalCounter.mayDecrement(result);
                        msgTotalCounter.mayDecrement(msg);
                        msgWEmailsCounter.mayDecrement(msg);
                        msgWLinksCounter.mayDecrement(msg);
                        msgWMentionsCounter.mayDecrement(msg)
                        msgWSumOver10KCounter.mayDecrement(msg);
                        msgWPings.mayDecrement(msg);
                    }, 2 * 1000)
                }).catch(err => console.log(err))
            }

            setTimeout(
                () => background.saveCounters(
                    {
                        total: msgTotalCounter.getCount(),
                        withEmails: msgWEmailsCounter.getCount(),
                        withLinks: msgWLinksCounter.getCount(),
                        withMentions: msgWMentionsCounter.getCount(),
                        withSumOver10K: msgWSumOver10KCounter.getCount(),
                        withPings: msgWPings.getCount(),
                    }
                )
            )
        } catch (e) {
            console.error(`Error at line ${e.lineNumber}: ${e.message}`);
        }
    })
}

initBot();
