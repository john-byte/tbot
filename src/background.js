const fs = require("fs");
const path = require("path");
const countersPath = path.resolve(
    __dirname,
    "..",
    "counters.json"
);

const loadCounters = async () => {
    const raw = await fs.promises.readFile(
        countersPath,
        {
            encoding: "utf-8"
        }
    );

    return await JSON.parse(raw);
}

const saveCounters = async (counters) => {
    const serialized = await JSON.stringify(counters, null, 2);
    await fs.promises.writeFile(
        countersPath,
        serialized,
        {
            encoding: "utf-8"
        }
    );
}

module.exports = {
    loadCounters,
    saveCounters
};
