/**
     * 
     * @param {string} char 
     * @returns if character is decimal digit
     */
const isDigit = (char) => "0".charCodeAt(0) <= char.charCodeAt(0) &&
    char.charCodeAt(0) <= "9".charCodeAt(0);

/**
* 
* @param {string} str string to be parsed
* @param {number} begin start index in string
* @returns floating point number representation of number string + end index
*/
const atof = (str, begin) => {
    if (
        !isDigit(str[begin]) &&
        str[begin] !== "+" &&
        str[begin] !== "-" &&
        str[begin] !== "."
    ) {
        return [0, begin + 1];
    }

    let result = 0;
    let sign = 1;
    let ptr = begin;

    if (str[ptr] === "+") {
        ptr++;
    } else if (str[ptr] === "-") {
        sign = -1;
        ptr++;
    }

    // Parsing integer part
    while (ptr < str.length && isDigit(str[ptr])) {
        result = result * 10 + (str.charCodeAt(ptr) - "0".charCodeAt(0));
        ptr++;
    }

    if (str[ptr] === ".") {
        ptr++;
        let pow = -1;

        while (ptr < str.length && isDigit(str[ptr])) {
            result += (str.charCodeAt(ptr) - "0".charCodeAt(0)) * Math.pow(10, pow);
            pow--;
            ptr++;
        }
    }

    return [result * sign, ptr];
}

module.exports = {
    atof
};
