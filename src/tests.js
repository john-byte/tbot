const atof = require("./utils").atof;

console.log(atof("99 bottles of the bear", 0));
console.log(atof("+12.34 and -5.6 farenheit", 0));
console.log(atof("+12.34 and -5.6 farenheit", 11));
console.log(atof("88005553535", 0));
